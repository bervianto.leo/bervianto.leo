# Hi there 👋

* I'm a __Software Engineer__ working in __Bandung, Indonesia__.
* I'm Tokino Sora's Fans.

<img src="./images/tokino_sora.png" width="300" height="300" alt="Tokino Sora" />

## Badges/Certifications

<!--START_SECTION:badges-->
[![HashiCorp Ambassador 2023](https://images.credly.com/size/110x110/images/692826e2-8c24-4967-8373-fa75319eaed8/image.png)](http://www.credly.com/badges/8cad11b0-12d7-4193-b51a-11a0c75de467 "HashiCorp Ambassador 2023")
[![.NET Workloads on Amazon ECS on AWS Fargate](https://images.credly.com/size/110x110/images/7e5e1967-439e-48e5-a913-625c712b2dc5/image.png)](http://www.credly.com/badges/1950c6d6-27ae-4e72-9c06-87bffee4fd9d ".NET Workloads on Amazon ECS on AWS Fargate")
[![.NET Workloads on AWS Lambda](https://images.credly.com/size/110x110/images/221e7d7f-bceb-422e-8c31-436ecbcda614/image.png)](http://www.credly.com/badges/e4dfc1aa-7adc-427a-aa6a-74bd89eb9574 ".NET Workloads on AWS Lambda")
[![AWS Learning: Architecting](https://images.credly.com/size/110x110/images/519a6dba-f145-4c1a-85a2-1d173d6898d9/image.png)](http://www.credly.com/badges/72a09770-9d56-428a-8304-383328c7057b "AWS Learning: Architecting")
[![Microsoft Certified: Security Operations Analyst Associate](https://images.credly.com/size/110x110/images/7e75516f-5149-4d19-8d09-aa3dab4907cb/security-operations-analyst-associate-600x600.png)](http://www.credly.com/badges/4136b42e-bf3d-447f-b8a8-9f884fed7cbd "Microsoft Certified: Security Operations Analyst Associate")
[![.NET Workloads on AWS App Runner](https://images.credly.com/size/110x110/images/eea64560-121f-4437-af9c-91cf20968d35/image.png)](http://www.credly.com/badges/e5e6cdf5-46d5-4927-87c4-67651ea9deed ".NET Workloads on AWS App Runner")
[![Microsoft Certified: Azure Cosmos DB Developer Specialty](https://images.credly.com/size/110x110/images/515fa1dc-ac4a-4f08-ac73-6fd9694124cb/image.png)](http://www.credly.com/badges/79854684-04d7-49df-835e-09ca5efa18c3 "Microsoft Certified: Azure Cosmos DB Developer Specialty")
[![Microsoft Certified: Azure Database Administrator Associate](https://images.credly.com/size/110x110/images/edc0b0d8-55ec-4dfe-9353-22c1bc4e07e8/azure-database-administrator-associate-600x600.png)](http://www.credly.com/badges/bb79f65b-29e8-44c9-8084-9b82293a4eea "Microsoft Certified: Azure Database Administrator Associate")
[![Cisco Certified DevNet Associate](https://images.credly.com/size/110x110/images/e21e94f7-feec-4717-9687-ac150b213f64/Cisco_DevNetAsst_600.png)](http://www.credly.com/badges/2eda7f15-37ae-4e21-a352-1366730dfcd8 "Cisco Certified DevNet Associate")
[![Microsoft Certified: Dynamics 365 Fundamentals (CRM)](https://images.credly.com/size/110x110/images/42992295-0ee2-4527-982d-e51efbec40fc/dynamics365-fundamentals-crm-600x600.png)](http://www.credly.com/badges/5d1f6458-4dc8-47fd-bc06-81a46f6124ae "Microsoft Certified: Dynamics 365 Fundamentals (CRM)")
[![AWS Academy Graduate - AWS Academy Cloud Architecting](https://images.credly.com/size/110x110/images/2f7b0627-48a0-4894-8d46-3245bdfe0463/image.png)](http://www.credly.com/badges/1ef1b3ca-06b9-4b35-99c2-f3e8a9967c6c "AWS Academy Graduate - AWS Academy Cloud Architecting")
[![Network Security](https://images.credly.com/size/110x110/images/f7387386-553c-4be5-b3f3-077f78152f31/Network_Security.png)](http://www.credly.com/badges/3638cf60-ee35-4017-a1cd-8d7058c4a18a "Network Security")
[![AWS Certified Developer – Associate](https://images.credly.com/size/110x110/images/b9feab85-1a43-4f6c-99a5-631b88d5461b/image.png)](http://www.credly.com/badges/675aece2-ca13-4995-959b-a799c3ed913b "AWS Certified Developer – Associate")
[![DevNet Associate](https://images.credly.com/size/110x110/images/35985f2b-38d6-4b6f-8e63-42b17d3b5c69/DEVASC_Learning_Badge.png)](http://www.credly.com/badges/6df128c6-1dad-4484-b2c3-6d95324353d7 "DevNet Associate")
[![AWS Certified Cloud Practitioner](https://images.credly.com/size/110x110/images/00634f82-b07f-4bbd-a6bb-53de397fc3a6/image.png)](http://www.credly.com/badges/9db987ad-5427-41a5-9942-1681ecd529ff "AWS Certified Cloud Practitioner")
[![Microsoft Certified: Identity and Access Administrator Associate](https://images.credly.com/size/110x110/images/91295436-0704-4b98-8e1a-ef5f937bda21/identity-and-access-administrator-associate-600x600.png)](http://www.credly.com/badges/28a220f0-a86d-4a28-b517-41c0d61a60d6 "Microsoft Certified: Identity and Access Administrator Associate")
[![Microsoft Certified: Security, Compliance, and Identity Fundamentals](https://images.credly.com/size/110x110/images/fc1352af-87fa-4947-ba54-398a0e63322e/security-compliance-and-identity-fundamentals-600x600.png)](http://www.credly.com/badges/b9e139a3-03d6-4669-a093-b02a55fa02dd "Microsoft Certified: Security, Compliance, and Identity Fundamentals")
[![LFC191: Open Source Licensing Basics for Software Developers](https://images.credly.com/size/110x110/images/4c76f677-fd18-4d7b-aec9-591123bfcc9a/Training_Badges_Master_osbestpractices.png)](http://www.credly.com/badges/e577cf75-f292-47a3-8256-b51efefd8e53 "LFC191: Open Source Licensing Basics for Software Developers")
[![Mapping & diagramming](https://images.credly.com/size/110x110/images/3cfab448-6ce8-402e-af8f-55956d4f19e3/Mapping___diagramming.png)](http://www.credly.com/badges/c91c915c-60be-459b-981a-f5d7d0b17942 "Mapping & diagramming")
[![Miro essentials](https://images.credly.com/size/110x110/images/80759b0b-b1e0-484d-aa28-01565d810780/Miro_essentials.png)](http://www.credly.com/badges/3f1ef803-d85f-4678-81cf-1b9f7eea3f91 "Miro essentials")
[![AWS Certified Solutions Architect – Associate](https://images.credly.com/size/110x110/images/0e284c3f-5164-4b21-8660-0d84737941bc/image.png)](http://www.credly.com/badges/fc08a77a-de57-484b-bc27-35f7f1d87489 "AWS Certified Solutions Architect – Associate")
[![Microsoft Certified: Power Platform Fundamentals](https://images.credly.com/size/110x110/images/2a6251f2-737b-4bf6-9190-d77570cc76fc/CERT-Fundamentals-Power-Platform.png)](http://www.credly.com/badges/b76e972c-c7fb-4365-a26b-fc670d9f251e "Microsoft Certified: Power Platform Fundamentals")
[![Microsoft Certified: Azure Solutions Architect Expert](https://images.credly.com/size/110x110/images/987adb7e-49be-4e24-b67e-55986bd3fe66/azure-solutions-architect-expert-600x600.png)](http://www.credly.com/badges/02dd9df4-a99f-4c9b-8655-19ecc9dbada6 "Microsoft Certified: Azure Solutions Architect Expert")
[![AZ-303: Microsoft Azure Architect Technologies](https://images.credly.com/size/110x110/images/285339cc-675a-4b1a-bdd9-283868af2fc8/EXAM-Expert-AZ-303-600x600.png)](http://www.credly.com/badges/024fdf0f-5df4-478d-a282-b73fad1ac02e "AZ-303: Microsoft Azure Architect Technologies")
[![AZ-304: Microsoft Azure Architect Design](https://images.credly.com/size/110x110/images/bfdff01e-a9dd-41fc-9301-8a90585c19bb/EXAM-Expert-AZ-304-600x600.png)](http://www.credly.com/badges/995c4047-f483-4ef9-9e72-0322dc66c9d8 "AZ-304: Microsoft Azure Architect Design")
[![Cisco Certified CyberOps Associate](https://images.credly.com/size/110x110/images/31459fb8-0734-4078-9175-dd1a6e56de4a/01_cyberops_associate_300.png)](http://www.credly.com/badges/f0a2f217-dd0d-4b35-87f7-1101d34085b5 "Cisco Certified CyberOps Associate")
[![LFS261: DevOps and SRE Fundamentals - Implementing Continuous Delivery](https://images.credly.com/size/110x110/images/fb65d04a-6138-4c71-b475-9c3851aea5df/LF_logobadge.png)](http://www.credly.com/badges/4a9b8c51-e933-49e5-95c3-0b57ece92f98 "LFS261: DevOps and SRE Fundamentals - Implementing Continuous Delivery")
[![LFS169: Introduction to GitOps](https://images.credly.com/size/110x110/images/5426612d-4ded-4408-bfaa-dbe3210f9cf9/LF_logobadge.png)](http://www.credly.com/badges/33fad6fc-258e-4189-8e1f-f5d3452d476c "LFS169: Introduction to GitOps")
[![Microsoft Certified: Azure Data Fundamentals](https://images.credly.com/size/110x110/images/70eb1e3f-d4de-4377-a062-b20fb29594ea/azure-data-fundamentals-600x600.png)](http://www.credly.com/badges/349eda96-0e21-4a83-83a2-d58914d3af90 "Microsoft Certified: Azure Data Fundamentals")
[![Microsoft Certified: Azure AI Fundamentals](https://images.credly.com/size/110x110/images/4136ced8-75d5-4afb-8677-40b6236e2672/azure-ai-fundamentals-600x600.png)](http://www.credly.com/badges/316cc5ea-57d7-4658-a2c1-c54976ad351d "Microsoft Certified: Azure AI Fundamentals")
[![Introduction to Cybersecurity](https://images.credly.com/size/110x110/images/af8c6b4e-fc31-47c4-8dcb-eb7a2065dc5b/I2CS__1_.png)](http://www.credly.com/badges/07b5abe4-6381-4205-9324-2e4b5301148b "Introduction to Cybersecurity")
[![CyberOps Associate](https://images.credly.com/size/110x110/images/53f37f83-04a1-4935-9b1e-21a99cc6e1b2/CyberOpsAssoc.png)](http://www.credly.com/badges/33982f50-ecf2-4363-8c64-0fb827e40bd6 "CyberOps Associate")
[![AZ-400: Designing and Implementing Microsoft DevOps Solutions](https://images.credly.com/size/110x110/images/107e2eb6-f394-40eb-83d2-d8c9b7d34555/exam-az400-600x600.png)](http://www.credly.com/badges/c11904b1-4d23-4623-9f2c-f50e9e283ff0 "AZ-400: Designing and Implementing Microsoft DevOps Solutions")
[![Microsoft Certified: DevOps Engineer Expert](https://images.credly.com/size/110x110/images/c3ab66f8-5d59-4afa-a6c2-0ba30a1989ca/CERT-Expert-DevOps-Engineer-600x600.png)](http://www.credly.com/badges/70b6559b-7762-41c5-a7e3-671723b0f4e5 "Microsoft Certified: DevOps Engineer Expert")
[![Microsoft Certified: Azure Developer Associate](https://images.credly.com/size/110x110/images/63316b60-f62d-4e51-aacc-c23cb850089c/azure-developer-associate-600x600.png)](http://www.credly.com/badges/161e6a46-79ba-4b2a-b98d-ce156266ebb9 "Microsoft Certified: Azure Developer Associate")
[![Microsoft Certified: Azure Fundamentals](https://images.credly.com/size/110x110/images/be8fcaeb-c769-4858-b567-ffaaa73ce8cf/image.png)](http://www.credly.com/badges/36c108ee-57dc-4d85-a669-72df190c4348 "Microsoft Certified: Azure Fundamentals")
<!--END_SECTION:badges-->

### Accredible / Google Cloud

<!--START_SECTION:accrediblebadges-->
[![Badge](https://api.accredible.com/v1/credential/generate_baked_badge?credential_id=57646449)](https://www.credential.net/7a3078e7-deb0-43e3-821f-adeed92da64d) [![Badge](https://api.accredible.com/v1/credential/generate_baked_badge?credential_id=49384407)](https://www.credential.net/a16eddfb-45d8-4e95-9e8e-b60dca6c310f)
<!--END_SECTION:accrediblebadges-->

* More [Badge/Certification](https://www.credly.com/users/bervianto-leo-pratama/badges)


## Focus & Interest Topic

| No | Focus | Interest Topic |
|:--:|:-----:|:--------------:|
| 1 | Microservices | Cyber security |
| 2 | Web API | Site Reliability Engineer (SRE) |
| 3 | Cloud Services / Cloud Computing | Cloud Architect |
| 4 | DevOps | DevSecOps |

## Focus Technology Stack

| No/Priority | Backend | Frontend | Database | Cloud Services |
|:--:|:-------:|:--------:|:--------:|:--------------:|
| 1 | ![C# (.NET, ASP.NET)](https://img.shields.io/badge/.NET-512BD4?style=for-the-badge&logo=dotnet&logoColor=white) ![C#](https://img.shields.io/badge/C%23-239120?style=for-the-badge&logo=c-sharp&logoColor=white) | ![Vue.js](https://img.shields.io/badge/Vue.js-35495E?style=for-the-badge&logo=vuedotjs&logoColor=4FC08D) | ![SQL Server](https://img.shields.io/badge/Microsoft%20SQL%20Server-CC2927?style=for-the-badge&logo=microsoft%20sql%20server&logoColor=white) | ![Azure](https://img.shields.io/badge/microsoft%20azure-0089D6?style=for-the-badge&logo=microsoft-azure&logoColor=white) |
| 2 | ![Node.js](https://img.shields.io/badge/Node.js-339933?style=for-the-badge&logo=nodedotjs&logoColor=white) | ![React.js](https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB) | ![Postgresql](https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white) | ![AWS](https://img.shields.io/badge/Amazon_AWS-FF9900?style=for-the-badge&logo=amazonaws&logoColor=white) |
| 3 | ![Python](https://img.shields.io/badge/Python-FFD43B?style=for-the-badge&logo=python&logoColor=blue) | ![Angular](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white) | ![MySQL](https://img.shields.io/badge/MySQL-005C84?style=for-the-badge&logo=mysql&logoColor=white) | ![Google Cloud](https://img.shields.io/badge/Google_Cloud-4285F4?style=for-the-badge&logo=google-cloud&logoColor=white) |
| 4 | ![Golang](https://img.shields.io/badge/Go-00ADD8?style=for-the-badge&logo=go&logoColor=white) | - | - | - |

## Contact & Website

### Contact

Need more information? Contact me:

| Social | Git Platform | Profesional Work | Video Platform | Forum |
|:------:|:------------:|:----------------:|:--------------:|:-----:|
| [![My Twitter][1.1]][1] [![Soundcloud][12.1]][12] [![Instagram][10.1]][10] | [![My Github][3.1]][3] | [![My Linkedin][4.1]][4] | [![My Twitch][5.1]][5] [![My Youtube][6.1]][6] | <a href="https://stackoverflow.com/users/6948591/bervianto-leo-pratama"><img src="https://stackoverflow.com/users/flair/6948591.png" width="208" height="58" alt="profile for Bervianto Leo Pratama at Stack Overflow, Q&amp;A for professional and enthusiast programmers" title="profile for Bervianto Leo Pratama at Stack Overflow, Q&amp;A for professional and enthusiast programmers"></a> |

### Website

| Blog | Personal Site |
|:----:|:-------------:|
| [![My Dev][7.1]][7] [![My Medium][8.1]][8] [![Hashnode][13.1]][13] | https://berviantoleo.my.id |

### More

[![Hackerrank][9.1]][9]
[![Sololearn][11.1]][11]

## Active Projects

| No | Name & Repo Link | Documentation | Demo | Package Manager Link |
|:--:|:----------------:|:-------------:|:----:|:--------------------:|
| 1 | [React Multi Crop](https://github.com/berviantoleo/react-multi-crop) | https://berviantoleo.github.io/react-multi-crop/ | https://react-multi-crop.netlify.app/ | [![React Multi Crop][20.1]][20] |
| 2 | [Feathers Advance Hooks](https://github.com/bervProject/feathers-advance-hook) | http://bervproject.berviantoleo.my.id/feathers-advance-hook/ | ... | [![Feathers Advance Hook][21.1]][21] |
| 3 | [BervProject.Validation.Common](https://github.com/bervProject/BervProject.Validation.Common) | https://bervproject.berviantoleo.my.id/BervProject.Validation.Common/ | ... | [![BervProject.Validation.Common][22.1]][22] |
| 4 | [BervProject.FeatureFlag](https://github.com/bervProject/BervProject.FeatureFlag) | https://bervproject.berviantoleo.my.id/BervProject.FeatureFlag/ | ... | ... |
| 5 | [feature-flag](https://github.com/berviantoleo/feature-flag) | https://gp.berviantoleo.my.id/feature-flag/ | ... | [![feature-flag][25.1]][25] |

## Support Me

[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I2YXS8)

## My Organization

[![bervProject][100.1]][100]

## Counter

![Counter](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgithub.com%2Fberviantoleo1212%2Fhit-counter)


[1.1]: https://img.shields.io/twitter/follow/berviantoleo?style=social (twitter icon)
[3.1]: https://img.shields.io/github/followers/berviantoleo?style=social (github icon)
[4.1]: https://img.shields.io/badge/linkedin-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white (linkedin icon)
[5.1]: https://img.shields.io/twitch/status/berviantoleo?style=social (twitch icon)
[6.1]: https://img.shields.io/youtube/channel/subscribers/UCAKsTh-TZRxy8I3AulBTFwA?style=social (youtube icon)
[7.1]: https://img.shields.io/badge/dev.to-0A0A0A?style=for-the-badge&logo=dev.to&logoColor=white (dev.to icon)
[8.1]: https://img.shields.io/badge/Medium-%23000000.svg?style=for-the-badge&logo=Medium&logoColor=white (medium icon)
[9.1]: https://img.shields.io/badge/-Hackerrank-2EC866?style=for-the-badge&logo=HackerRank&logoColor=white (hackerrank icon)
[10.1]: https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white (instagram icon)
[11.1]: https://img.shields.io/badge/-Sololearn-3a464b?style=for-the-badge&logo=Sololearn&logoColor=white (sololearn icon)
[12.1]: https://img.shields.io/badge/SoundCloud-FF3300?style=for-the-badge&logo=soundcloud&logoColor=white (soundcloud icon)
[13.1]: https://img.shields.io/badge/Hashnode-2962FF?style=for-the-badge&logo=hashnode&logoColor=white (hashnode icon)

[20.1]: https://img.shields.io/npm/v/@berviantoleo/react-multi-crop (npm react multi crop icon)
[21.1]: https://img.shields.io/npm/v/@bervproject/feathers-advance-hook (npm feathers advance hook icon)
[22.1]: https://img.shields.io/nuget/v/BervProject.Validation.Common (nuget BervProject.Validation.Common)
[25.1]: https://img.shields.io/npm/v/@berviantoleo/feature-flag (npm feature-flag icon)

[100.1]: https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white (github project icon)



[1]: https://twitter.com/berviantoleo
[3]: https://github.com/berviantoleo
[4]: https://linkedin.com/in/bervianto-leo-pratama
[5]: https://www.twitch.tv/berviantoleo
[6]: https://www.youtube.com/channel/UCAKsTh-TZRxy8I3AulBTFwA
[7]: https://dev.to/berviantoleo
[8]: https://berviantoleo.medium.com
[9]: https://www.hackerrank.com/berviantoleo
[10]: https://instagram.com/bervianto.leo
[11]: https://www.sololearn.com/profile/937172
[12]: https://soundcloud.com/bervianto-leo-pratama
[13]: https://hashnode.berviantoleo.my.id/

[20]: https://www.npmjs.com/package/@berviantoleo/react-multi-crop
[21]: https://www.npmjs.com/package/@bervproject/feathers-advance-hook
[22]: https://www.nuget.org/packages/BervProject.Validation.Common/
[25]: https://www.npmjs.com/package/@berviantoleo/feature-flag

[100]: https://github.com/bervProject
